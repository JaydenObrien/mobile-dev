package com.example.jayden.assignment3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button bTemp;
    private Button bDist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init()
    {
        bTemp = (Button) findViewById(R.id.Temp);
        bDist = (Button) findViewById(R.id.Dist);
        final Intent dist = new Intent(this, DistanceConverter.class);
        final Intent temp = new Intent(this, TempConverter.class);
        bDist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(dist);
            }
        });

        bTemp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(temp);
            }
        });
    }
}
