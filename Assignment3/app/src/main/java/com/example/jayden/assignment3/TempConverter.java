package com.example.jayden.assignment3;

import android.content.Context;
import android.graphics.Rect;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class TempConverter extends AppCompatActivity {

    static final String Cel = "Cel";
    private float Celsius;
    private Button bConvert;
    private EditText txtInput;
    private TextView tvOutput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_converter);
        init();
        if (savedInstanceState != null)
        {
            Celsius = savedInstanceState.getFloat(Cel);
            refresh();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putFloat(Cel,Celsius);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }

    private  void init(){
        bConvert = (Button) findViewById(R.id.Convert);
        txtInput = (EditText) findViewById(R.id.Input);
        tvOutput = (TextView) findViewById(R.id.Output);
        Celsius = 0.0f;
        bConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getStateOfValues();
            }
        });
    }

    private void getStateOfValues()
    {
        float far = 0.0f;
        try {
            if (!txtInput.getText().toString().equals(null) && !txtInput.getText().toString().equals("")) {
                Celsius = Float.parseFloat(txtInput.getText().toString());
            }
            far = (Celsius * 1.8f) + 32;
            tvOutput.setText(far + " °F");
        } catch (Exception e){
            tvOutput.setText("Error");
        }
    }

    private void refresh()
    {
        float far = (Celsius *1.8f) + 32;
        tvOutput.setText(far+ " °F");
    }
}
