package com.example.jayden.assignment3;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class DistanceConverter extends AppCompatActivity {

    static final String CM = "CM";
    static final String M = "M";
    static final String MM = "MM";
    static final String KM = "KM";
    static final String SEL = "SEL";

    private String selected;
    private double miles;
    private double feet;
    private double inches;
    private double cm;
    private double m;
    private double mm;
    private double km;
    private EditText txtMiles;
    private EditText txtFeet;
    private EditText txtInches;
    private Button bConvert;
    private Spinner sUnits;
    private TextView tvOutput;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance_converter);
        init();
        if (savedInstanceState != null)
        {
            mm = savedInstanceState.getDouble(MM);
            cm = savedInstanceState.getDouble(CM);
            m = savedInstanceState.getDouble(M);
            km = savedInstanceState.getDouble(KM);
            selected = savedInstanceState.getString(SEL);
            changeUnits(selected);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putDouble(CM,cm);
        savedInstanceState.putDouble(M,m);
        savedInstanceState.putDouble(KM,km);
        savedInstanceState.putDouble(MM,mm);
        savedInstanceState.putString(SEL,selected);
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    private void init()
    {
        txtMiles = (EditText)findViewById(R.id.Miles);
        txtFeet = (EditText)findViewById(R.id.Feet);
        txtInches = (EditText)findViewById(R.id.Inc);
        bConvert = (Button)findViewById(R.id.button);
        sUnits = (Spinner) findViewById(R.id.conversions);
        tvOutput = (TextView)findViewById(R.id.textView);
        //spinner init
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.Conversions, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sUnits.setAdapter(adapter);
        bConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getStateOfValues();
            }
        });

        sUnits.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selected = sUnits.getSelectedItem().toString();
                changeUnits(selected);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    //magic code that i pull from net that closes keyboard win clicked on screen
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }

    private void getStateOfValues()
    {
        double inc;

        miles =0;
        feet = 0;
        inches = 0;
        try {
            if (!txtMiles.getText().toString().equals(null) && !txtMiles.getText().toString().equals("")) {
                miles = Double.parseDouble(txtMiles.getText().toString());
            }
            if (!txtFeet.getText().toString().equals(null) && !txtFeet.getText().toString().equals("")) {
                feet = Double.parseDouble(txtFeet.getText().toString());
            }
            if (!txtInches.getText().toString().equals(null) && !txtInches.getText().toString().equals("")) {
                inches = Double.parseDouble(txtInches.getText().toString());
            }
            inc = miles * 63360 + feet * 12 + inches;
            mm = inc * 25.4;
            cm = mm / 10;
            m = cm / 100;
            km = m / 1000;
            selected = sUnits.getSelectedItem().toString();
            changeUnits(selected);
        } catch (Exception e){
            tvOutput.setText("Error");
        }
    }

    private void changeUnits(String str)
    {
        if (str.equals("Metres"))
            tvOutput.setText(String.valueOf(Math.round(m*100000d)/100000d) + "m");
        else if (str.equals("Centimetres"))
            tvOutput.setText(String.valueOf(Math.round(cm*100000d)/100000d) + "cm");
        else if (str.equals("Millimeters"))
            tvOutput.setText(String.valueOf(Math.round(mm*100000d)/100000d) + "mm");
        else if (str.equals("Kilometers"))
            tvOutput.setText(String.valueOf(Math.round(km*100000d)/100000d) + "km");
    }

}
