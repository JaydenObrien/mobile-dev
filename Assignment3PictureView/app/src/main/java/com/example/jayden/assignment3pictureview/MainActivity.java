package com.example.jayden.assignment3pictureview;

import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.net.URI;

public class MainActivity extends AppCompatActivity {

    private ImageView img1;
    private ImageView img2;
    private ImageView img3;
    private ImageView img4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init()
    {
        img1 = (ImageView)findViewById(R.id.cat1);
        img2 = (ImageView)findViewById(R.id.cat2);
        img3 = (ImageView)findViewById(R.id.cat3);
        img4 = (ImageView)findViewById(R.id.cat4);
        final Intent intent = new Intent(this, image_preview.class);
        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("resourse", R.drawable.cat_bowl);
                intent.putExtra("des",img1.getContentDescription());
                startActivity(intent);
            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("resourse", R.drawable.cat);
                intent.putExtra("des",img2.getContentDescription());
                startActivity(intent);
            }
        });

        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("resourse", R.drawable.cat_tie);
                intent.putExtra("des",img3.getContentDescription());
                startActivity(intent);
            }
        });
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("resourse", R.drawable.cat_sleepy);
                intent.putExtra("des",img4.getContentDescription());
                startActivity(intent);
            }
        });
    }


}
