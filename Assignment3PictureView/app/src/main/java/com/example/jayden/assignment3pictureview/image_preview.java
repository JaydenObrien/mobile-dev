package com.example.jayden.assignment3pictureview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class image_preview extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);

        Bundle extras = getIntent().getExtras();
        if(extras == null)
            return;

        TextView descrip = (TextView) findViewById(R.id.ImageDescription);
        ImageView view = (ImageView) findViewById(R.id.image);
        descrip.setText(extras.getString("des"));
        view.setImageResource(extras.getInt("resourse"));

        view.setScaleType(ImageView.ScaleType.FIT_CENTER);


    }
}
