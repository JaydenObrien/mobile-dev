package com.example.jayden.assignment4;

import android.app.ActionBar;
import android.content.Intent;
import android.media.Image;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.security.spec.ECField;
import java.util.Date;


public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 1;
    private ImageView image1;
    private ImageView image2;
    private ImageView image3;
    private ImageView image4;
    private TextView tvImg1;
    private TextView tvImg2;
    private TextView tvImg3;
    private TextView tvImg4;
    private TextView time1;
    private TextView time2;
    private TextView time3;
    private TextView time4;
    private Imagedata imgdata1;
    private Imagedata imgdata2;
    private Imagedata imgdata3;
    private Imagedata imgdata4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try
        {
            init();
        } catch(Exception e)
        {
            Log.e("error",e.toString());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK)
        {
            if(data.hasExtra("data")){
                switch(requestCode)
                {
                    case 1: imgdata1 = data.getParcelableExtra("data");
                            tvImg1.setText(imgdata1.getName());
                            time1.setText(imgdata1.getDate());
                            break;
                    case 2: imgdata2 = data.getParcelableExtra("data");
                            tvImg2.setText(imgdata2.getName());
                            time2.setText(imgdata2.getDate());
                            break;
                    case 3: imgdata3 = data.getParcelableExtra("data");
                            tvImg3.setText(imgdata3.getName());
                            time3.setText(imgdata3.getDate());
                            break;
                    case 4: imgdata4 = data.getParcelableExtra("data");
                            tvImg4.setText(imgdata4.getName());
                            time4.setText(imgdata4.getDate());
                            break;
                    default: //shits fucked
                            break;
                }

            }
        }

    }

    private void init(){
        imgdata1 = new Imagedata(1,"test", "loc", new String[] {"Hello", "words"}, "1/11/11",true,"Hello@email.com",0);
        imgdata2 = new Imagedata(2,"test", "loc", new String[] {"Hello", "words"}, "1/11/11",true,"Hello@email.com",0);
        imgdata3 = new Imagedata(3,"test", "loc", new String[] {"Hello", "words"}, "1/11/11",true,"Hello@email.com",0);
        imgdata4 = new Imagedata(4,"test", "loc", new String[] {"Hello", "words"}, "1/11/11",true,"Hello@email.com",0);

        image1 = (ImageView) findViewById(R.id.imageView1);
        image2 = (ImageView) findViewById(R.id.imageView2);
        image3 = (ImageView) findViewById(R.id.imageView3);
        image4 = (ImageView) findViewById(R.id.imageView4);
        tvImg1 = (TextView)  findViewById(R.id.name1);
        tvImg2 = (TextView)  findViewById(R.id.name2);
        tvImg3 = (TextView)  findViewById(R.id.name3);
        tvImg4 = (TextView)  findViewById(R.id.name4);
        time1 = (TextView) findViewById(R.id.time1);
        time2 = (TextView) findViewById(R.id.time2);
        time3 = (TextView) findViewById(R.id.Time3);
        time4 = (TextView) findViewById(R.id.time4);
        final Intent intent = new Intent(this, ImageForm.class);

        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("data", imgdata1);
                startActivityForResult(intent,1);
            }
        });
        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("data", imgdata2);
                startActivityForResult(intent,2);
            }
        });
        image3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("data", imgdata3);
                startActivityForResult(intent,3);
            }
        });
        image4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("data", imgdata4);
                startActivityForResult(intent,4);
            }
        });

        image1.setImageResource(R.drawable.tiger);
        image2.setImageResource(R.drawable.carrots);
        image3.setImageResource(R.drawable.flower);
        image4.setImageResource(R.drawable.dog);
        tvImg1.setText(imgdata1.getName());
        tvImg2.setText(imgdata2.getName());
        tvImg3.setText(imgdata3.getName());
        tvImg4.setText(imgdata4.getName());
        time1.setText(imgdata1.getDate());
        time2.setText(imgdata2.getDate());
        time3.setText(imgdata3.getDate());
        time4.setText(imgdata4.getDate());
    }
}
