package com.example.jayden.assignment4;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Parcel;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

public class ImageForm extends AppCompatActivity {
    private EditText name;
    private EditText url;
    private EditText keys;
    private EditText email;
    private RatingBar stars;
    private CheckBox share;
    private TextView date;
    private Imagedata data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_form);
        date = (TextView) findViewById(R.id.date);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "datePicker");
            }
        });
        try
        {
            Bundle extras = getIntent().getExtras();
            data = extras.getParcelable("data");
            init();

        } catch (Exception e)
        {

            Log.e("error",e.toString());
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (name.getText().toString().isEmpty())
        {
            Toast toast = Toast.makeText(getApplicationContext(),"Enter a name",Toast.LENGTH_LONG);
            toast.show();
            return true;
        }
        if(!isValidEmail(email.getText().toString()))
        {
            Toast toast = Toast.makeText(getApplicationContext(),"Enter a valid email",Toast.LENGTH_LONG);
            toast.show();
            return true;
        }
        String[] tokens = keys.getText().toString().split(",");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent();
                Imagedata temp = new Imagedata(data.getCode(),name.getText().toString(),url.getText().toString(),tokens, date.getText().toString(),share.isChecked(),email.getText().toString(),stars.getRating());
                intent.putExtra("data",temp);
                setResult(RESULT_OK,intent);

                finish();
                break;
        }
        return true;

    }

    private void init()
    {
        name = (EditText)findViewById(R.id.Name);
        url = (EditText)findViewById(R.id.URL);
        keys = (EditText)findViewById(R.id.keywords);
        //date = (EditText)findViewById(R.id.date);
        email = (EditText)findViewById(R.id.email);
        stars = (RatingBar) findViewById(R.id.rating);
        share = (CheckBox) findViewById(R.id.Share);

        StringBuilder keywordOut = new StringBuilder();
        name.setText(data.getName());
        url.setText(data.getLoc());
        for (String keys:data.getKeywords())
        {
            keywordOut.append(keys);
            keywordOut.append(",");
        }
        keys.setText(keywordOut);
        date.setText(data.getDate().toString());
        email.setText(data.getEmail());
        stars.setRating(data.getRating());
        share.setChecked(data.isShare());

    }

    public TextView getDate()
    {
        return date;
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


}
