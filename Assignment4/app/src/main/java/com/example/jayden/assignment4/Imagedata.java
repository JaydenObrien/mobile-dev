package com.example.jayden.assignment4;

import android.os.Parcel;
import android.os.Parcelable;

import java.net.URL;
import java.util.Date;

public class Imagedata implements Parcelable {
    public int getCode() {
        return code;
    }

    private int code;
    private String name;
    private String loc;
    private String[] keywords;
    private String date;
    private boolean share;
    private String email;
    private float rating;

    public Imagedata(Parcel parcel)
    {
        int code = parcel.readInt();
        name = parcel.readString();
        loc = parcel.readString();
        keywords = parcel.createStringArray();
        date = parcel.readString();
        share = (boolean)parcel.readValue(null);
        email = parcel.readString();
        rating = parcel.readFloat();
    }

    //, String loc, String[] keywords, Date date, boolean share, String email, float rating
    public Imagedata(int code, String name,String loc, String[] keywords, String date, boolean share, String email, float rating)
    {
        this.code = code;
        this.name = name;
        this.loc = loc;
        this.keywords = keywords;
        this.date = date;
        this.share = share;
        this.email = email;
        this.rating = rating;
    }


    public static final Parcelable.Creator<Imagedata> CREATOR  =
    new Parcelable.Creator<Imagedata>() {

        public Imagedata createFromParcel(Parcel parcel) {
            return new Imagedata(parcel);
        }

        public Imagedata[] newArray(int size) {
            return new Imagedata[0];
        }
    };

    @Override
    public int describeContents() {
        return hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(code);
        parcel.writeString(name);
        parcel.writeString(loc);
        parcel.writeStringArray(keywords);
        parcel.writeString(date);
        parcel.writeValue(share);
        parcel.writeString(email);
        parcel.writeFloat(rating);
    }
    public String getName() {
        return name;
    }

    public String getLoc() {
        return loc;
    }

    public String[] getKeywords() {
        return keywords;
    }

    public String getDate() {
        return date;
    }

    public boolean isShare() {
        return share;
    }

    public String getEmail() {
        return email;
    }

    public float getRating() {
        return rating;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public void setKeywords(String[] keywords) {
        this.keywords = keywords;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setShare(boolean share) {
        this.share = share;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
