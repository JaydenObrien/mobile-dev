package com.example.jayden.assignment2;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    static final String CM = "CM";
    static final String M = "M";
    static final String CHECK = "CHECKED";

    private double miles;
    private double feet;
    private double inches;
    private double cm;
    private double m;
    private EditText txtMiles;
    private EditText txtFeet;
    private EditText txtInches;
    private Button bConvert;
    private CheckBox cMetres;
    private TextView tvOutput;
    boolean isChecked;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        if (savedInstanceState != null)
        {
            cm = savedInstanceState.getDouble(CM);
            m = savedInstanceState.getDouble(M);
            isChecked = savedInstanceState.getBoolean(CHECK);
            changeUnits();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putDouble(CM,cm);
        savedInstanceState.putDouble(M,m);
        savedInstanceState.putBoolean(CHECK, isChecked);
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    private void init()
    {
        isChecked = false;
        txtMiles = (EditText)findViewById(R.id.Miles);
        txtFeet = (EditText)findViewById(R.id.Feet);
        txtInches = (EditText)findViewById(R.id.Inc);
        bConvert = (Button)findViewById(R.id.button);
        cMetres = (CheckBox)findViewById(R.id.checkBox);
        tvOutput = (TextView)findViewById(R.id.textView);

        bConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getStateOfValues();
            }
        });

        cMetres.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                isChecked = !isChecked;
                changeUnits();
            }
        });

    }

    //magic code that i pull from net that closes keyboard win clicked on screen
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }

    private void getStateOfValues()
    {
        double inc;

        miles =0;
        feet = 0;
        inches = 0;
        if (!txtMiles.getText().toString().equals(null) && !txtMiles.getText().toString().equals(""))
        {
            miles = Double.parseDouble(txtMiles.getText().toString());
        }
        if (!txtFeet.getText().toString().equals(null) && !txtFeet.getText().toString().equals(""))
        {
            feet = Double.parseDouble(txtFeet.getText().toString());
        }
        if (!txtInches.getText().toString().equals(null) && !txtInches.getText().toString().equals(""))
        {
            inches = Double.parseDouble(txtInches.getText().toString());
        }
        inc = miles*5280 + feet*12 + inches;
        cm = inc*2.54;
        m = cm/100;
        if (cMetres.isChecked())
            tvOutput.setText(String.valueOf(Math.round(m*100000d)/100000d) + "m");
        else
            tvOutput.setText(String.valueOf(Math.round(cm*100000d)/100000d) + "cm");
    }

    private void changeUnits()
    {
        if (isChecked)
            tvOutput.setText(String.valueOf(Math.round(m*100000d)/100000d) + "m");
        else
            tvOutput.setText(String.valueOf(Math.round(cm*100000d)/100000d) + "cm");
    }


}
