package swindroid.suntime.calc;

/**
 * Created by Jayden on 7/10/2017.
 */

public class Location {
    private String city;
    private float longitude;
    private float latitude;
    private String State;


    public Location(String city, float longitude, float latitude, String State)
    {
        this.city = city;
        this.longitude = longitude;
        this.latitude = latitude;
        this.State = State;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }
}
