package swindroid.suntime.ui;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Vector;
import java.util.TimeZone;
import swindroid.suntime.R;
import swindroid.suntime.calc.AstronomicalCalendar;
import swindroid.suntime.calc.GeoLocation;
import swindroid.suntime.calc.Location;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.Spinner;
import android.widget.TextView;

public class Main extends Activity 
{
    private HashMap<String, Vector<Location>> locData = new HashMap<String, Vector<Location>>();
	private Spinner states;
	private Spinner cities;
	private Location current;
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		LoadLocationFile();
		initializeUI();
    }

	private void initializeUI()
	{
		current = locData.get("VIC").get(0);
		DatePicker dp = (DatePicker) findViewById(R.id.datePicker);
		Calendar cal = Calendar.getInstance();
		final int year = cal.get(Calendar.YEAR);
		final int month = cal.get(Calendar.MONTH);
		final int day = cal.get(Calendar.DAY_OF_MONTH);
		dp.init(year,month,day,dateChangeHandler); // setup initial values and reg. handler
		updateTime(year, month, day,current);

		states = (Spinner) findViewById(R.id.state);
		cities = (Spinner) findViewById(R.id.cities);

		ArrayAdapter<CharSequence> adap = ArrayAdapter.createFromResource(this,R.array.states, android.R.layout.simple_spinner_item);
		adap.setDropDownViewResource(android.R.layout.simple_spinner_item);
		states.setAdapter(adap);
		states.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				setAdapterForCities(states.getSelectedItem().toString());
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});

		cities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				current = locData.get(states.getSelectedItem().toString()).get(cities.getSelectedItemPosition());
				updateTime(year,month,day,current);
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});
	}

	private void setAdapterForCities(String key)
	{
		String[] memes = new String[locData.get(key).size()];
		int i = 0;
		for (Location l: locData.get(key))
		{
			memes[i] = l.getCity();
			i++;
		}
		ArrayAdapter citersArray = new ArrayAdapter(this, android.R.layout.simple_spinner_item, memes);
		citersArray.setDropDownViewResource(R.layout.simple_spinner_item);
		cities.setAdapter(citersArray);
	}

	private void updateTime(int year, int monthOfYear, int dayOfMonth, Location loc)
	{
		TimeZone tz = TimeZone.getTimeZone(loc.getState());
		GeoLocation geolocation = new GeoLocation(loc.getCity(), loc.getLongitude(), loc.getLatitude(), tz);
		AstronomicalCalendar ac = new AstronomicalCalendar(geolocation);
		ac.getCalendar().set(year, monthOfYear, dayOfMonth);
		Date srise = ac.getSunrise();
		Date sset = ac.getSunset();
		
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		
		TextView sunriseTV = (TextView) findViewById(R.id.sunriseTimeTV);
		TextView sunsetTV = (TextView) findViewById(R.id.sunsetTimeTV);
		Log.d("SUNRISE Unformatted", srise+"");
		
		sunriseTV.setText(sdf.format(srise));
		sunsetTV.setText(sdf.format(sset));		
	}


	private void LoadLocationFile()
	{
		Vector<Location> VIC = new Vector<Location>();
		Vector<Location> NSW = new Vector<Location>();
		Vector<Location> QLD = new Vector<Location>();
		Vector<Location> SA = new Vector<Location>();
		Vector<Location> NT = new Vector<Location>();
		Vector<Location> WA = new Vector<Location>();
		Vector<Location> TAS = new Vector<Location>();
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(getApplicationContext().getResources().openRawResource(R.raw.au_loc)));
			String line = reader.readLine();

			while (line !=null)
			{
				String[] parsed = line.split(",");

				if (parsed[3].equals("Australia/Adelaide"))
				{
					SA.addElement(new Location(parsed[0],Float.valueOf(parsed[1]), Float.valueOf(parsed[2]), parsed[3]));
				} else if(parsed[3].equals("Australia/Perth"))
				{
					WA.addElement(new Location(parsed[0],Float.valueOf(parsed[1]), Float.valueOf(parsed[2]), parsed[3]));
				} else if (parsed[3].equals("Australia/Brisbane"))
				{
					QLD.addElement(new Location(parsed[0],Float.valueOf(parsed[1]), Float.valueOf(parsed[2]), parsed[3]));
				} else if (parsed[3].equals("Australia/Darwin"))
				{
					NT.addElement(new Location(parsed[0],Float.valueOf(parsed[1]), Float.valueOf(parsed[2]), parsed[3]));
				} else if (parsed[3].equals("Australia/Sydney"))
				{
					NSW.addElement(new Location(parsed[0],Float.valueOf(parsed[1]), Float.valueOf(parsed[2]), parsed[3]));
				} else if (parsed[3].equals("Australia/Melbourne"))
				{
					VIC.addElement(new Location(parsed[0],Float.valueOf(parsed[1]), Float.valueOf(parsed[2]), parsed[3]));
				} else if (parsed[3].equals("Australia/Hobart"))
				{
					TAS.addElement(new Location(parsed[0],Float.valueOf(parsed[1]), Float.valueOf(parsed[2]), parsed[3]));
				}
				line = reader.readLine();
			}

			locData.put("VIC", VIC);
			locData.put("NSW", NSW);
			locData.put("QLD", QLD);
			locData.put("SA", SA);
			locData.put("WA", WA);
			locData.put("TAS", TAS);
			locData.put("NT", NT);
		} catch (Exception e)
		{
			Log.e("filememes", e.toString());
		}

	}

	OnDateChangedListener dateChangeHandler = new OnDateChangedListener()
	{
		public void onDateChanged(DatePicker dp, int year, int monthOfYear, int dayOfMonth)
		{
			updateTime(year, monthOfYear, dayOfMonth, current);
		}	
	};
	
}